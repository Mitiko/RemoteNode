const app = require('express')()

const port = 8080

app.post('/send', (req, res) => {
  console.log('POST')
  console.log(req.body)
  res.send(req.body.data)
})

app.get('/receive', (req, res) => {
  res.end('Hello server!')
})

app.listen(port, () => {
  console.log('Listening on port ' + port);
})
